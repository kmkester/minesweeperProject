package com.example.minesweeper;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Grid {
    private int size;
    private List<Tile> tiles;
    private Context con;
    private LinearLayout tileLayout;


    public Grid(Context context, int size){
        con = context;
        this.size = size;
        tiles = new ArrayList<>();
        for(int i = 0; i < size * size; i++){
            tiles.add(new Tile(Tile.NOTHING, con));
        }
    }

    public int toIndex(int x, int y){
         int col = size * y;
         return col + x;
    }
    public int[] toCoords(int index){
        int y = index / size;       //col
        int x = index - y * size;   //row
        int[] coords = new int[]{x, y};
        return coords;
    }

    public void makeMinefield(int numMines){
        int placed = 0;            //places 1 too few mines? set to -1 to account
        while(placed < numMines){
            int x = new Random().nextInt(size);
            int y = new Random().nextInt(size);

            if(!tiles.get(toIndex(x, y)).hasBomb()){
                tiles.get(toIndex(x, y)).giveBomb();
                placed++;
            }
        }
    }

    private void adjMines(int x, int y){
        int sumMines = 0;
        if(indexExists(x-1, y-1)){
            if(tiles.get(toIndex(x-1, y-1)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x, y-1)){
            if(tiles.get(toIndex(x, y-1)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x+1, y-1)){
            if(tiles.get(toIndex(x+1, y-1)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x-1, y)){
            if(tiles.get(toIndex(x-1, y)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x+1, y)){
            if(tiles.get(toIndex(x+1, y)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x-1, y+1)){
            if(tiles.get(toIndex(x-1, y+1)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x, y+1)){
            if(tiles.get(toIndex(x, y+1)).hasBomb()){
                sumMines++;
            }
        }
        if(indexExists(x+1, y+1)){
            if(tiles.get(toIndex(x+1, y+1)).hasBomb()){
                sumMines++;
            }
        }
//        if(!tiles.get(toIndex(x, y)).hasBomb()){
            tiles.get(toIndex(x, y)).setTileStatus(sumMines);
//        }
    }
    public boolean indexExists(int x, int y){
        if(x >= 0 && x < size && y >= 0 && y < size){
            return true;
        }
        return false;
    }
    public void runAdjMines(){
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                adjMines(i, j);
            }
        }
    }

    public List<Tile> getTiles(){
        return tiles;
    }
    public int getSize(){
        return size;
    }

    //public Grid getGrid() {return }
}
