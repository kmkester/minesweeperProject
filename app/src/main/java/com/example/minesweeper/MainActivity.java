package com.example.minesweeper;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    //timer and save timer data
    //disable shake
    private TextView mostMinesText, largestSizeText;
    private static int mostMines, largestSize;
    private static int gridSize;
    private static int numMines;
    private Button playButt;
    private EditText numMinesEditText, gridSizeEditText;
    private TextView errorText;
    private static int tileSize;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_screen);

        mostMinesText = findViewById(R.id.highScoreText);
        largestSizeText = findViewById(R.id.lastScoreText);
        SharedPreferences sharedPreferences = getSharedPreferences("SHARED_PREFS", MODE_PRIVATE);
        mostMines = sharedPreferences.getInt("mostMines", 0);
        largestSize = sharedPreferences.getInt("largestSize", 0);
        mostMinesText.setText("Most Mines Cleared: " + mostMines);
        largestSizeText.setText("Largest Size Cleared: " + largestSize);

        numMinesEditText = findViewById(R.id.numMinesEdit);
        gridSizeEditText = findViewById(R.id.gridSizeEdit);

        errorText = findViewById(R.id.errorText);

        playButt = findViewById(R.id.playButt);
        playButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                errorText.setVisibility(View.INVISIBLE);
                gridSize = Integer.valueOf(gridSizeEditText.getText().toString());
                numMines = Integer.valueOf(numMinesEditText.getText().toString());
                if(numMines < gridSize * gridSize && gridSize < 11 && gridSize > 1 && numMines > 0){
                    tileSize = gridSize * -6 + 100; //size scaling
                    startActivity(new Intent(MainActivity.this, GameActivity.class));
                }
                else if(gridSize <= 1){   //invalid
                    errorText.setText("Grid size is too small.");
                    errorText.setVisibility(View.VISIBLE);
                }
                else if(gridSize >= 11){
                    errorText.setText("Grid size is too large.");
                    errorText.setVisibility(View.VISIBLE);
                }
                else if(numMines >= gridSize * gridSize){
                    errorText.setText("Too many mines");
                    errorText.setVisibility(View.VISIBLE);
                }
                else if(numMines <= 0){
                    errorText.setText("Too few mines");
                    errorText.setVisibility(View.VISIBLE);
                }
                else {
                    errorText.setText("ERROR: bad input values");
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    public static int getGridSize(){
        return gridSize;
    }
    public static int getNumMines(){
        return numMines;
    }
    public static int getTileSize(){
        return tileSize;
    }
    public static int getMostMines(){
        return mostMines;
    }
    public static int getLargestSize(){
        return largestSize;
    }
}