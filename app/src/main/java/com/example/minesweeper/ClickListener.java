package com.example.minesweeper;

public interface ClickListener {
    void onTileClicked(Tile tile);
}
