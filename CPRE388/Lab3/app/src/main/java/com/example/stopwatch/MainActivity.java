package com.example.stopwatch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView display;
    private Button resetButton;
    private Button startStopButton;

    private boolean isStart = true;

    private int hr = 0, min = 0, sec = 0, tenths = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityViewModel viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        final Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                display.setText((viewModel.getTimeint()));
            }
        };


        display = findViewById(R.id.timerText);
        resetButton = findViewById(R.id.resetButt);
        startStopButton = findViewById(R.id.startStopButt);

        display.setText(viewModel.timeMS);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.resetTime();
            }
        });

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isStart) {
                    startStopButton.setText("Stop");
                    isStart = false;

                    viewModel.startTime();
//                    viewModel.getTime().observe(this, Observer{
//
//                    });
                }
                else{
                    startStopButton.setText("Start");
                    isStart = true;

                    viewModel.pauseTime();
                }
            }
        });
    }
}