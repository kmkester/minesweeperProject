package com.example.lab3stopwatch;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.os.SystemClock;

import android.os.Handler;
import java.util.logging.LogRecord;

public class MainActivityViewModel extends ViewModel {
    long now, startTime, currTime, prevTime = 0;
    boolean isRunning = false;
//    Handler handler = new Handler();
//
//    Runnable ticker = new Runnable(){
//        @Override
//        public void run() {
//            now = SystemClock.uptimeMillis();
//
//            handler.postAtTime(ticker, now);
//
//        }
//    };
//    public long getCurrTime(){
//        currTime = SystemClock.uptimeMillis();
//        return currTime;
//    }
    private MutableLiveData<Long> elapsedTime;
        public MutableLiveData<Long> getElapsedTime(){
            if(elapsedTime == null){
                elapsedTime = new MutableLiveData<Long>();
            }
            return elapsedTime;
        }

        public void resetTime(){
            prevTime = 0;
            elapsedTime.setValue(0L);
        }
        public void startTime(){
            isRunning = true;
            startTime = SystemClock.uptimeMillis();  //TODO: figure out how to actually update clock


            Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(isRunning){
                        elapsedTime.setValue(SystemClock.uptimeMillis() - startTime + prevTime);
                    }
                    handler.postDelayed(this, 10);
                }
            });
            //put this in handler to act like loop?
        }
        public void stopTime(){
            prevTime = SystemClock.uptimeMillis() - startTime;
            isRunning = false;

            //handler.postAtTime(null, now);
        }

//        public void setElapsedTime(int input){
//            elapsedTime.setValue((long) input);
//        }
}
