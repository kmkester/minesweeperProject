package com.example.lab3stopwatch;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private MainActivityViewModel viewModel;

    private TextView display;
    private Button resetButton;
    private Button startStopButton;
    private int hr, min, sec, ms;
    private long time, startTime;
    private String formattedTime = "";
    private boolean isStart = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.timerText);
        resetButton = findViewById(R.id.resetButt);
        startStopButton = findViewById(R.id.startStopButt);

        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //time = 0;
                //viewModel.getElapsedTime().setValue(0L);
                //viewModel.setElapsedTime(0);
                viewModel.resetTime();
            }
        });

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isStart) {
                    startStopButton.setText("Stop");
                    isStart = false;
                    viewModel.startTime();
                }
                else{
                    startStopButton.setText("Start");
                    isStart = true;
                    viewModel.stopTime();
                }
            }
        });

        final Observer<Long> timerObserver = new Observer<Long>() {
            @Override
            public void onChanged(@Nullable final Long totalTime) {
                time = totalTime;
                hr = (int) (time / (1000*60*60));
                time -= hr * (1000*60*60);
                min = (int) time / (1000*60);
                time -= min * (1000*60);
                sec = (int) time / 1000;
                time -= sec * 1000;
                ms = (int) time;
                while (ms >= 10){           //gets 1st digit of ms
                    ms /= 10;
                }


                formattedTime = String.format("%02d:%02d:%02d.%1d", hr,min,sec,ms);

                display.setText(formattedTime.toString());
            }
        };

        viewModel.getElapsedTime().observe(this, timerObserver);
    }
}