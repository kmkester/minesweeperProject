package com.example.whackamole;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * This class is coordinated with the xml file of the menu screen. It's the start/end screen and
 * displays the previous and high scores with a play button. Also loads shared preference values.
 */
public class MainActivity extends AppCompatActivity {

    Button playButton;
    TextView highScore, lastScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_screen);

        playButton = findViewById(R.id.playButt);
        highScore = findViewById(R.id.highScoreTextMenu);
        lastScore = findViewById(R.id.lastScoreText);

        SharedPreferences sharedPreferences = getSharedPreferences("SHARED_PREFS", MODE_PRIVATE);
        WhackViewModel.lastScore = sharedPreferences.getInt("lastScore", 0);
        WhackViewModel.highScore = sharedPreferences.getInt("highScore", 0);

        highScore.setText("High Score: " + WhackViewModel.highScore);
        lastScore.setText("Last Score: " + WhackViewModel.lastScore);

//        SharedPreferences sharedPreferences = getSharedPreferences("SHARED_PREFS", MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();

//        editor.putInt("lastScore", 0);
//        editor.putInt("highScore", 0);
//        editor.apply();

        /**
         * This is the listener for the play button, when it's pressed, it starts the whack time
         * activity.
         */
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WhackTime.stop = false;
                startActivity(new Intent(MainActivity.this, WhackTime.class));
                //setContentView(R.layout.whack_time);
            }
        });
    }
}