package com.example.whackamole;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.os.Handler;

/**
 * This class is the actual game activity. It includes all of the holes and moles and some of the
 * logic behind visibility is included in here as well.
 */
public class WhackTime extends AppCompatActivity{

    ImageView hole, mole0, mole1, mole2, mole3, mole4, mole5, mole6, mole7, mole8, hole0, hole1, hole2, hole3, hole4, hole5, hole6, hole7, hole8;
    TextView highScoreText, currentScoreText, livesText;
    MediaPlayer mp, oof;
    LinearLayout background;
    public static boolean stop = false;
    private WhackViewModel viewModel;
    private WhackViewModel thread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whack_time);

        mole0 = findViewById(R.id.mole0);
        mole1 = findViewById(R.id.mole1);
        mole2 = findViewById(R.id.mole2);
        mole3 = findViewById(R.id.mole3);
        mole4 = findViewById(R.id.mole4);
        mole5 = findViewById(R.id.mole5);
        mole6 = findViewById(R.id.mole6);
        mole7 = findViewById(R.id.mole7);
        mole8 = findViewById(R.id.mole8);

        hole0 = findViewById(R.id.hole0);
        hole1 = findViewById(R.id.hole1);
        hole2 = findViewById(R.id.hole2);
        hole3 = findViewById(R.id.hole3);
        hole4 = findViewById(R.id.hole4);
        hole5 = findViewById(R.id.hole5);
        hole6 = findViewById(R.id.hole6);
        hole7 = findViewById(R.id.hole7);
        hole8 = findViewById(R.id.hole8);



        SharedPreferences sharedPreferences = getSharedPreferences("SHARED_PREFS", MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
//        WhackViewModel.lastScore = sharedPreferences.getInt("lastScore", 0);
//        WhackViewModel.highScore = sharedPreferences.getInt("highScore", 0);

        background = (LinearLayout)findViewById(R.id.background);

        highScoreText = findViewById(R.id.highScoreText);
        currentScoreText = findViewById(R.id.currScoreText);
        livesText = findViewById(R.id.livesText);

        mp = MediaPlayer.create(this, R.raw.molesound);
        oof = MediaPlayer.create(this, R.raw.roblox);
        viewModel = new ViewModelProvider(this).get(WhackViewModel.class);
        //thread = new WhackViewModel();

        Handler handler = new Handler();
        final int[] scoreValue = {0};
        final long[] startTime = {0};
        int delayTime = 2500 - 5* WhackViewModel.currScore; //speeds up delay time, might need a cap
        currentScoreText.setText("Score: " + WhackViewModel.currScore);
        highScoreText.setText("High Score: " + WhackViewModel.highScore);
        livesText.setText("Lives: " + WhackViewModel.numLives);
        mole0.setVisibility(View.VISIBLE);


        /**
         * If the player takes too long to click, then remove a life because of this miss
         */
        //replace with runnable, call postDelay, removeCallback to this to stop it from running
        Runnable missedRunnable = new Runnable() {
            @Override
            public void run() {
//                long timeMillis = SystemClock.uptimeMillis() - startTime[0];
//                if(WhackViewModel.currScore > 300){
//                    scoreValue[0] = 400;
//                }
//                else{
//                    scoreValue[0] = WhackViewModel.currScore*2;
//                }
                //if(timeMillis > 1000 - scoreValue[0]){     //when missed mole
                    //handler.removeCallbacks(this); //stop the runnable
                    int delayTime = 2500 - 5* WhackViewModel.currScore;
                    WhackViewModel.moleMissed();
                    livesText.setText("Lives: " + WhackViewModel.numLives);
                    oof.start();
                    WhackViewModel.falseMoleList();
                    mole0.setVisibility(View.INVISIBLE);
                    mole1.setVisibility(View.INVISIBLE);
                    mole2.setVisibility(View.INVISIBLE);
                    mole3.setVisibility(View.INVISIBLE);
                    mole4.setVisibility(View.INVISIBLE);
                    mole5.setVisibility(View.INVISIBLE);
                    mole6.setVisibility(View.INVISIBLE);
                    mole7.setVisibility(View.INVISIBLE);
                    mole8.setVisibility(View.INVISIBLE);

                WhackViewModel.updateMoleList();
                if(WhackViewModel.numLives < 1){
                    stop = true;
                    WhackViewModel.gameEnd();
                    editor.putInt("lastScore", WhackViewModel.lastScore);
                    editor.putInt("highScore", WhackViewModel.highScore);
                    editor.apply();

                    startActivity(new Intent(WhackTime.this, MainActivity.class));
                }
                if(WhackViewModel.getMoleList(0)){
                    mole0.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(1)){
                    mole1.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(2)){
                    mole2.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(3)){
                    mole3.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(4)){
                    mole4.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(5)){
                    mole5.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(6)){
                    mole6.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(7)){
                    mole7.setVisibility(View.VISIBLE);
                }
                if(WhackViewModel.getMoleList(8)){
                    mole8.setVisibility(View.VISIBLE);
                }
                if(!stop) {
                    handler.postDelayed(this, delayTime);
                }
                }
               // handler.postDelayed(this, 10);
            //}
        };
//        background.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                WhackViewModel.moleMissed();
//                livesText.setText("Lives: " + WhackViewModel.numLives);
//                oof.start();
//                WhackViewModel.falseMoleList();
//                    mole0.setVisibility(View.INVISIBLE);
//                    mole1.setVisibility(View.INVISIBLE);
//                    mole2.setVisibility(View.INVISIBLE);
//                    mole3.setVisibility(View.INVISIBLE);
//                    mole4.setVisibility(View.INVISIBLE);
//                    mole5.setVisibility(View.INVISIBLE);
//                    mole6.setVisibility(View.INVISIBLE);
//                    mole7.setVisibility(View.INVISIBLE);
//                    mole8.setVisibility(View.INVISIBLE);
//
//                WhackViewModel.updateMoleList();
//                if(WhackViewModel.numLives == 0){
//                    WhackViewModel.gameEnd();
//                    editor.putInt("lastScore", WhackViewModel.lastScore);
//                    editor.putInt("highScore", WhackViewModel.highScore);
//                    editor.apply();
//                    startActivity(new Intent(WhackTime.this, MainActivity.class));
//                }
//                if(WhackViewModel.getMoleList(0)){
//                    mole0.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(1)){
//                    mole1.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(2)){
//                    mole2.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(3)){
//                    mole3.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(4)){
//                    mole4.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(5)){
//                    mole5.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(6)){
//                    mole6.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(7)){
//                    mole7.setVisibility(View.VISIBLE);
//                }
//                if(WhackViewModel.getMoleList(8)){
//                    mole8.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        /**
         * When a mole is pressed, that's when a tick starts. This controls the visibility of the
         * moles and asks the view model for helpful public variables.
         */
        mole0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);

                mole0.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(0);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
               // handler.postDelayed(missedRunnable,delayTime);
                //handler.removeCallbacks(missedRunnable);
//                if(WhackViewModel.checkTwoMoles()){
//
//                }
            }
        });
        mole1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                //
                mole1.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(1);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.postDelayed(missedRunnable,delayTime);
                //handler.removeCallbacks(missedRunnable);
            }
        });
        mole2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                mole2.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(2);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.removeCallbacks(missedRunnable);
                //handler.postDelayed(missedRunnable,delayTime);
            }
        });
        mole3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                //
                mole3.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(3);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.postDelayed(missedRunnable,delayTime);
                //handler.removeCallbacks(missedRunnable);
            }
        });
        mole4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                //
                mole4.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(4);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.removeCallbacks(missedRunnable);
                //handler.postDelayed(missedRunnable,delayTime);
            }
        });
        mole5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                //handler.removeCallbacks(missedRunnable);
                mole5.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(5);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.removeCallbacks(missedRunnable);
                //handler.postDelayed(missedRunnable,delayTime);
            }
        });
        mole6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);

                mole6.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(6);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.removeCallbacks(missedRunnable);
                //handler.postDelayed(missedRunnable,delayTime);
            }
        });
        mole7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                //handler.removeCallbacks(missedRunnable);
                mole7.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(7);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);   //5 seconds
                startTime[0] = SystemClock.uptimeMillis();
                //handler.removeCallbacks(missedRunnable);
                //handler.postDelayed(missedRunnable,delayTime);
            }
        });
        mole8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacks(missedRunnable);
                handler.postDelayed(missedRunnable,delayTime);
                mole8.setVisibility(View.INVISIBLE);
                viewModel.moleClicked(8);
                currentScoreText.setText("Score: " + WhackViewModel.currScore);
                if (WhackViewModel.currScore > WhackViewModel.highScore){
                    WhackViewModel.highScore = WhackViewModel.currScore;
                    highScoreText.setText("High Score: " + WhackViewModel.highScore);
                }
                mp.start();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(WhackViewModel.getMoleList(0)){
                            mole0.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(1)){
                            mole1.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(2)){
                            mole2.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(3)){
                            mole3.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(4)){
                            mole4.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(5)){
                            mole5.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(6)){
                            mole6.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(7)){
                            mole7.setVisibility(View.VISIBLE);
                        }
                        if(WhackViewModel.getMoleList(8)){
                            mole8.setVisibility(View.VISIBLE);
                        }
                    }
                }, WhackViewModel.timeMS+10);
                startTime[0] = SystemClock.uptimeMillis();
               // handler.removeCallbacks(missedRunnable);
            }
        });
    }
}
