package com.example.stepcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView textView;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private int numSteps = 0;
    private int numTicks = 0;
    private Handler handler;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        textView = findViewById(R.id.numSteps);
        button = findViewById(R.id.reset);
        //handler = new Handler();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numTicks = 0;
                numSteps = 0;
            }
        });
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        // The light sensor returns a single value.
        // Many sensors return 3 values, one for each axis.
        //float lux = event.values[0];
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        if (Math.abs(y - 9.8) > 4 && Math.abs(x - 9.8) > 4 && Math.abs(z - 9.8) > 4) {
            numTicks++;
        }
        if(numTicks == 12){
            numTicks = 0;
            numSteps++;
        }
        textView.setText("Steps:\n" + numSteps);
    }
//        handler.postDelayed(new Runnable() {
//            public void run() {
//
//                }
//            }
//
//            }, 400);

        // Do something with this sensor value.
        //textView.setText("X: " + x + "\n" + "Y: " + y + "\n" + "Z: " + z);


    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

}