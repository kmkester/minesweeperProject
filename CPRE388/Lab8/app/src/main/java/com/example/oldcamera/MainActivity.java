package com.example.oldcamera;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;

import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

    private Camera mCamera;
    private Camera.Parameters mParams;
    private int rotation;
    private int angle;
    private SurfaceView surfaceView;
    private SurfaceHolder mHolder;
    private final int REQUEST_CAMERA_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        surfaceView = findViewById(R.id.surfaceView);
        if (ContextCompat.checkSelfPermission(

                this, Manifest.permission.CAMERA) ==

                PackageManager.PERMISSION_GRANTED) {

            // You can use the API that requires the permission.



        } else {

            // You can directly ask for the permission.

            ActivityCompat.requestPermissions(this,

                    new String[] { Manifest.permission.CAMERA },

                    1);

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        mCamera = Camera.open();
        mParams = mCamera.getParameters();
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_90:
                angle = 90;
                break;
            case Surface.ROTATION_180:
                angle = 180;
                break;
            case Surface.ROTATION_270:
                angle = 270;
                break;
            default:
                angle = 0;
                break;
        }
        mCamera.setDisplayOrientation(angle);

        //mHolder.addCallback((SurfaceHolder.Callback) this); //dunno how this works
        mHolder = surfaceView.getHolder();
        try {
            while(!mHolder.getSurface().isValid()){

            }
            mCamera.setPreviewDisplay(mHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mCamera.startPreview();
        surfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //take pic and resume, all null but last
            }
        });
    }
}