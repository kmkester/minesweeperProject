package com.example.minesweeper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GameActivity extends AppCompatActivity implements ClickListener, SensorEventListener {
    private RecyclerView recyclerView;
    private SweeperRecyclerAdapter recyclerAdapter;
    private Grid grid;
    private Button flagButton;
    private TextView flagText;
    private View tileView;
    private boolean sweepMode = true;
    private boolean isRunning = true;
    private int minesFlagged = 0, flagsUsed = 0; //flagsUsed is what is displayed in activity, minesFlagged is on gameEnd
    private int tilesCleared = 0;
    private LinearLayout tileLayout;

    private SoundPool soundPool;
    private int winSound, clickSound, bombSound, flagSound;
    private int soundStreamID;

    private SharedPreferences sharedPreferences;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private long lastUpdate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);

        flagText = findViewById(R.id.minesText);
        flagButton = (Button) findViewById(R.id.flagToggleButt);

        flagText.setText(flagsUsed + "/" + MainActivity.getNumMines() + " Mines");
        grid = new Grid(this, MainActivity.getGridSize());
        grid.makeMinefield(MainActivity.getNumMines());
        grid.runAdjMines();

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, MainActivity.getGridSize()));

        recyclerAdapter = new SweeperRecyclerAdapter(grid.getTiles(), this);
        recyclerView.setAdapter(recyclerAdapter);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(4)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
        }

        bombSound = soundPool.load(this, R.raw.bomb, 1);
        flagSound = soundPool.load(this, R.raw.flag, 1);
        clickSound = soundPool.load(this, R.raw.tile, 1);
        winSound = soundPool.load(this, R.raw.win, 1);

        flagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sweepMode){
                    flagButton.setText("Flag Mode");
                    sweepMode = false;
                }
                else{
                    flagButton.setText("Sweep Mode");
                    sweepMode = true;
                }
                flagText.setText(flagsUsed + "/" + MainActivity.getNumMines() + " Mines");
            }
        });
    }

    @Override
    public void onTileClicked(Tile tile) {
        //Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
        clickTile(tile);
        recyclerAdapter.changeTiles(grid.getTiles());
    }

    private void clickTile(Tile tile){
        int index = grid.getTiles().indexOf(tile);
        int[] coords = grid.toCoords(index);
        if(sweepMode && !tile.isFlagged() && isRunning){
            if(tile.getTileStatus() == Tile.NOTHING && !tile.hasBomb()){
                checkAdjBlank(tile, coords[0], coords[1]);
            }
            else if(tile.hasBomb()){
                isRunning = false;
                endGame(recyclerView, false);
            }
            else{               //click good tile that has a bomb by it
                tile.show();
                tilesCleared++;
                soundPool.play(clickSound, 1, 1, 0, 0, 1);
                //soundPool.autoPause();
                if(MainActivity.getGridSize() * MainActivity.getGridSize() - (tilesCleared + MainActivity.getNumMines()) <= 0){
                    endGame(recyclerView, true);
                }
            }
        }
        else if(!sweepMode && !tile.isShown() && isRunning){           //if click on tile in flag mode
            if(tile.isFlagged()){   //click on flagged tile
                tile.unFlag();
                flagsUsed--;
                if(tile.hasBomb()){
                    minesFlagged--;
                }
                soundPool.play(flagSound, 1, 1, 0, 0, 1);
            }
            else if(MainActivity.getNumMines() - flagsUsed > 0){    //if there are enough flags
                tile.flag();
                flagsUsed++;
                if(tile.hasBomb()){
                    minesFlagged++;
                }
//                if(minesFlagged == MainActivity.getNumMines()){
//                    endGame(recyclerView, true);
//                }
                soundPool.play(flagSound, 1, 1, 0, 0, 1);
            }
            flagText.setText(flagsUsed + "/" + MainActivity.getNumMines() + " Mines");
        }
    }

    private void checkAdjBlank(Tile tile, int x, int y){
        if(!grid.indexExists(x, y)){
            return;
        }
        int index = grid.toIndex(x, y);
        int[] coords = grid.toCoords(index);
        Tile newTile = grid.getTiles().get(index);
        if(!newTile.isShown() && tile.getTileStatus() == Tile.NOTHING){
            newTile.show();
            tilesCleared++;
            if(MainActivity.getGridSize() * MainActivity.getGridSize() - (tilesCleared + MainActivity.getNumMines()) <= 0){
                endGame(recyclerView, true);
                return; //might not need this?
            }
            soundPool.play(clickSound, 1, 1, 0, 0, 1);
            checkAdjBlank(newTile, coords[0]-1, coords[1]);
            checkAdjBlank(newTile, coords[0]+1, coords[1]);
            checkAdjBlank(newTile, coords[0], coords[1]-1);
            checkAdjBlank(newTile, coords[0], coords[1]+1);

            checkAdjBlank(newTile, coords[0]-1, coords[1]-1);
            checkAdjBlank(newTile, coords[0]-1, coords[1]+1);
            checkAdjBlank(newTile, coords[0]+1, coords[1]-1);
            checkAdjBlank(newTile, coords[0]+1, coords[1]+1);
        }
    }

    private void endGame(View view, boolean wonGame){
        Tile tile;
        for(int i = 0; i < MainActivity.getGridSize() * MainActivity.getGridSize(); i++){
            tile = grid.getTiles().get(i);
            if(tile.hasBomb()){
                tile.show();
            }
        }
        //popup stuff
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.game_over_popup, null);
        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = false; // lets taps outside the popup NOT dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window token
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        TextView gameOverText = (TextView) popupView.findViewById(R.id.gameOverText);
        Button menuButton = (Button) popupView.findViewById(R.id.menuButt);
        Button tryAgainButton = (Button) popupView.findViewById(R.id.againButt);

        if(!wonGame){
            soundPool.play(bombSound, 1, 1, 0, 0, 1);
            gameOverText.setText("     Game Over!\n" + minesFlagged + "/" + MainActivity.getNumMines() + " Mines Flagged\n" + tilesCleared+ "/" + MainActivity.getGridSize() * MainActivity.getGridSize() + " Tiles Cleared");
        }
        else{
            soundPool.play(winSound, 1, 1, 0, 0, 1);
            gameOverText.setText("You win!");

            sharedPreferences = getSharedPreferences("SHARED_PREFS", MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            if(MainActivity.getMostMines() < MainActivity.getNumMines()){
                editor.putInt("mostMines", MainActivity.getNumMines());
                editor.apply();
            }
            if(MainActivity.getLargestSize() < MainActivity.getGridSize()){
                editor.putInt("largestSize", MainActivity.getGridSize());
                editor.apply();
            }
        }

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                startActivity(new Intent(GameActivity.this, MainActivity.class));
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                startActivity(new Intent(GameActivity.this, GameActivity.class));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundPool.release();
        soundPool = null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int numTicks = 0;
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        if (Math.abs(y - 9.8) > 15 && Math.abs(x - 9.8) > 15 && Math.abs(z - 9.8) > 15) {
            //if(Math.abs(y - 9.8) > 1){ // for emulator
            long actualTime = event.timestamp; //get the event's timestamp
            if (actualTime - lastUpdate > 800000000) {
                lastUpdate = actualTime;

                if (sweepMode) {
                    flagButton.setText("Flag Mode");
                    sweepMode = false;
                } else {
                    flagButton.setText("Sweep Mode");
                    sweepMode = true;
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}
