package com.example.minesweeper;

import android.content.res.Resources;
import android.graphics.Color;
import android.location.GnssAntennaInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SweeperRecyclerAdapter extends RecyclerView.Adapter<SweeperRecyclerAdapter.TileViewHolder> {

    private List<Tile> tiles;
    private ClickListener listener;

    public SweeperRecyclerAdapter(List<Tile> tiles, ClickListener listener){
        this.tiles = tiles;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View tileView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tile, parent, false);
        return new TileViewHolder(tileView);
    }

    @Override
    public void onBindViewHolder(@NonNull TileViewHolder holder, int position) {
        holder.bind(tiles.get(position));
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return tiles.size();
    }

    public void changeTiles(List<Tile> tiles){
        this.tiles = tiles;
        notifyDataSetChanged();
    }

    class TileViewHolder extends RecyclerView.ViewHolder{
        TextView tileDisplay;

        public TileViewHolder(@NonNull View tileView) {
            super(tileView);
            tileDisplay = tileView.findViewById(R.id.tileValue);

            tileDisplay.setTextSize(MainActivity.getTileSize()/2);
            ViewGroup.LayoutParams params = tileDisplay.getLayoutParams();

            params.width = MainActivity.getTileSize() * 3;
            params.height = MainActivity.getTileSize() * 3;
            tileDisplay.setLayoutParams(params);

        }

        public void bind(Tile tile){
            tileDisplay.setBackgroundColor(Color.GRAY);

            tileDisplay.setOnClickListener((view -> {
                listener.onTileClicked(tile);
                    })
            );
            if(tile.isShown()){
                if(tile.hasBomb()){
                    //tileDisplay.setBackgroundColor(Color.GREEN);
                    tileDisplay.setText("*");
                    tileDisplay.setTextColor(Color.RED);
                }
                else if(tile.getTileStatus() == Tile.NOTHING){
                    tileDisplay.setBackgroundColor(Color.TRANSPARENT);
                }
                else{
                    tileDisplay.setText(String.valueOf(tile.getTileStatus()));
                    if(tile.getTileStatus() == 1){
                        tileDisplay.setTextColor(Color.BLUE);
                    }
                    if(tile.getTileStatus() == 2){
                        tileDisplay.setTextColor(Color.GREEN);
                    }
                    if(tile.getTileStatus() == 3){
                        tileDisplay.setTextColor(Color.RED);
                    }
                    if(tile.getTileStatus() == 4){
                        tileDisplay.setTextColor(Color.MAGENTA); //purple
                    }
                    if(tile.getTileStatus() == 5){
                        tileDisplay.setTextColor(Color.DKGRAY); //maroon
                    }
                    if(tile.getTileStatus() == 6){
                        tileDisplay.setTextColor(Color.CYAN);
                    }
                    if(tile.getTileStatus() == 7){
                        tileDisplay.setTextColor(Color.BLACK);
                    }
                    if(tile.getTileStatus() == 8){
                        tileDisplay.setTextColor(Color.LTGRAY);
                    }
                }
            }
            else if(tile.isFlagged()){
                //tileDisplay.setBackgroundColor();
                tileDisplay.setTextColor(Color.YELLOW);
                tileDisplay.setText("F");
            }
        }
    }
}
