package edu.iastate.netid.pocketcalculator;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity<clearButton> extends AppCompatActivity {
    /**
     * The instance of the calculator model for use by this controller.
     */
    private final CalculationStream mCalculationStream = new CalculationStream();

    /*
     * The instance of the calculator display TextView. You can use this to update the calculator display.
     */
    private TextView mCalculatorDisplay;
    private Button clearButton;
    private Button oneButton;
    private Button twoButton;
    private Button threeButton;
    private Button fourButton;
    private Button fiveButton;
    private Button sixButton;
    private Button sevenButton;
    private Button eightButton;
    private Button nineButton;
    private Button zeroButton;
    private Button timesButton;
    private Button minusButton;
    private Button plusButton;
    private Button divideButton;
    private Button equalsButton;
    private Button decimalButton;

    //private boolean clear=false, one=false, two=false, three=false, four=false, five=false, six=false, seven=false, eight=false, nine=false, zero=false, times=false, minus=false, plus=false, divide=false, equals=false, decimal=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* uncomment the below line after you make your layout. This line locates
           the instance of the calculator display's TextView.  You need to create this TextView
           and set its ID to CalculatorDisplay in your layout resource file.
         */
        mCalculatorDisplay = findViewById(R.id.CalculatorDisplay);
        clearButton = findViewById(R.id.clearButt);
        oneButton = findViewById(R.id.oneButt);
        twoButton = findViewById(R.id.twoButt);
        threeButton = findViewById(R.id.threeButt);
        fourButton = findViewById(R.id.fourButt);
        fiveButton = findViewById(R.id.fiveButt);
        sixButton = findViewById(R.id.sixButt);
        sevenButton = findViewById(R.id.sevenButt);
        eightButton = findViewById(R.id.eightButt);
        nineButton = findViewById(R.id.nineButt);
        zeroButton = findViewById(R.id.zeroButt);
        timesButton = findViewById(R.id.timesButt);
        minusButton = findViewById(R.id.minusButt);
        plusButton = findViewById(R.id.plusButt);
        divideButton = findViewById(R.id.divideButt);
        equalsButton = findViewById(R.id.equalsButt);
        decimalButton = findViewById(R.id.decimalButt);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClearClicked(view);
            }
        });
        oneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOneClicked(view);
            }
        });
        twoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTwoClicked(view);
            }
        });
        threeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onThreeClicked(view);
            }
        });
        fourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFourClicked(view);
            }
        });
        fiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFiveClicked(view);
            }
        });
        sixButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSixClicked(view);
            }
        });
        sevenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSevenClicked(view);
            }
        });
        eightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEightClicked(view);
            }
        });
        nineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNineClicked(view);
            }
        });
        zeroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onZeroClicked(view);
            }
        });
        timesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTimesClicked(view);
            }
        });
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMinusClicked(view);
            }
        });
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPlusClicked(view);
            }
        });
        divideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDivideClicked(view);
            }
        });
        equalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEqualClicked(view);
            }
        });
        decimalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDecimalClicked(view);
            }
        });
    }

    /* TODO - add event listeners for your calculator's buttons. See the model's API to figure out
       what methods should be called. The equals button event listener has been done for you. Once
       you've created the layout, you'll need to add these methods as the onClick() listeners
       for the corresponding buttons in the XML layout. */

    public void onClearClicked (View view){
        try {
            mCalculationStream.clear();
        } finally {
            updateCalculatorDisplay();
        }
    }
    public void onOneClicked (View view){
        try {
            mCalculationStream.inputDigit(CalculationStream.Digit.ONE);
        } finally {
            updateCalculatorDisplay();
        }
    }
    public void onTwoClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.TWO);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onThreeClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.THREE);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onFourClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.FOUR);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onFiveClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.FIVE);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onSixClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.SIX);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onSevenClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.SEVEN);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onEightClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.EIGHT);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onNineClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.NINE);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onZeroClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.ZERO);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onDecimalClicked(View view) {
        try{
            mCalculationStream.inputDigit(CalculationStream.Digit.DECIMAL);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onTimesClicked(View view) {
        try{
            mCalculationStream.inputOperation(CalculationStream.Operation.MULTIPLY);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onPlusClicked(View view) {
        try{
            mCalculationStream.inputOperation(CalculationStream.Operation.ADD);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onMinusClicked(View view) {
        try{
            mCalculationStream.inputOperation(CalculationStream.Operation.SUBTRACT);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onDivideClicked(View view) {
        try{
            mCalculationStream.inputOperation(CalculationStream.Operation.DIVIDE);
        } finally{
            updateCalculatorDisplay();
        }
    }
    public void onEqualClicked(View view) {
        try {
            mCalculationStream.calculateResult();
        } finally {
            updateCalculatorDisplay();
        }
    }

    /**
     * Call this method after every button press to update the text display of your calculator.
     */
    public void updateCalculatorDisplay() {
        String value = getString(R.string.empty);
        try {
            value = Double.toString(mCalculationStream.getCurrentOperand());
        } catch(NumberFormatException e) {
            value = getString(R.string.error);
        } finally {
            // TODO: this value may need to be formatted first so it fits on one line... try 0.8 - 0.2
            if (value.toString().length() > 12){
                value = "Error";
            }
            mCalculatorDisplay.setText(value);
        }
    }
}
